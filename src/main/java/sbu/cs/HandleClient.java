package sbu.cs;

import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.Socket;

public class HandleClient {
    private Socket socket = null;
    private String filepath;
    private int port;
    private InetAddress IP;
    private InputStream in;
    private DataOutputStream out;

    public HandleClient(String filepath,InetAddress IP, int port) {
        this.filepath = filepath;
        this.port = port;
        this.IP = IP;

    }
    public void Build() throws IOException {
        try{
            socket = new Socket(IP,port);
            in = new FileInputStream(filepath);
            out = new DataOutputStream(socket.getOutputStream());
        }
        catch (IOException e) {
            System.out.println(e);
        }
    }
    public void WriteName() throws IOException {
        try {
            out.writeUTF(filepath);
        }
        catch (IOException e) {
            System.out.println(e);
        }
    }
    public void WriteFile() throws IOException {
        try {
            byte[] buffer = new byte[16*1024];
            int count;
            while ((count = in.read(buffer)) > 0)
            {
                out.write(buffer, 0, count);
            }
        }
        catch (IOException e) {
            System.out.println(e);
        }
    }
    public void Close() throws IOException {
        try {
            out.flush();
            out.close();
            in.close();
            socket.close();
        }
        catch (IOException e) {
            System.out.println(e);
        }
    }
}

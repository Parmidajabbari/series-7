package sbu.cs;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Client {

    /**
     * Create a socket and connect to server then send fileName and after that send file to be saved on server side.
     *  in here filePath == fileName
     *
     * @param args a string array with one element
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        String filePath = args[0];      // "sbu.png" or "book.pdf"
        try{
                HandleClient handleClient = new HandleClient(filePath,InetAddress.getLocalHost(),8000);
                handleClient.Build();
                handleClient.WriteName();
                handleClient.WriteFile();
                handleClient.Close();
        }
        catch (IOException e) {
            System.out.println(e);
        }


    }
}

package sbu.cs;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class HandleServer {
    private ServerSocket serverSocket = null;
    private Socket socket = null;
    private String directory;
    private int port;
    private DataInputStream in;
    private OutputStream out;

    public HandleServer(String directory,int port) {
        this.directory = directory;
        this.port = port;
    }
    public void Build()throws IOException {
        try{
           serverSocket = new ServerSocket(port);
           socket = serverSocket.accept();
           in = new DataInputStream(socket.getInputStream());
        }
        catch (IOException e) {
            System.out.println(e);
        }
    }
    public void HandleDirectory() {
        File theDir = new File(directory);
        if (!theDir.exists()){
            theDir.mkdirs();
        }
    }
    public void ReadName() throws IOException{
        try{
            String name = in.readUTF();
            out = new FileOutputStream(directory +  "/" + name);
        }
        catch (IOException e) {
            System.out.println(e);
        }
    }
    public void ReadFile()throws IOException {
        try{
            byte[] bytes = new byte[16*1024];
            int count;
            while ((count = in.read(bytes)) > 0) {
                out.write(bytes, 0, count);
            }
        }
        catch (IOException e) {
            System.out.println(e);
        }
    }
    public void Close() throws IOException {
        try {
            in.close();
            out.close();
            socket.close();
        }
        catch (IOException e) {
            System.out.println(e);
        }
    }

}
